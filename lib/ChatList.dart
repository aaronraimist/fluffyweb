import 'package:famedlysdk/famedlysdk.dart';
import 'package:fluffyweb/AdaptivePageLayout.dart';
import 'package:fluffyweb/Avatar.dart';
import 'package:fluffyweb/Chat.dart';
import 'package:fluffyweb/FadeRoute.dart';
import 'package:fluffyweb/Login.dart';
import 'package:fluffyweb/Matrix.dart';
import 'package:flutter/material.dart';

class ChatList extends StatefulWidget {
  final String activeChat;

  const ChatList({this.activeChat, Key key}) : super(key: key);
  @override
  _ChatListState createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  RoomList roomList;
  Client client;

  Future<List<Room>> getRooms(BuildContext context) async {
    if (roomList != null) return roomList.rooms;
    if (client.prevBatch?.isEmpty ?? true)
      await client.connection.onFirstSync.stream.first;
    roomList = client.getRoomList(onUpdate: () {
      setState(() {});
    });
    return roomList.rooms;
  }

  @override
  void dispose() {
    roomList?.eventSub?.cancel();
    roomList?.firstSyncSub?.cancel();
    roomList?.roomSub?.cancel();
    super.dispose();
  }

  void startChatDialog() {
    showDialog(
      context: context,
      builder: (BuildContext innerContext) {
        final TextEditingController controller = TextEditingController();
        return AlertDialog(
          title: Text("New private chat"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextField(
                controller: controller,
                autofocus: true,
                autocorrect: false,
                decoration: InputDecoration(
                    labelText: "Enter a username",
                    icon: Icon(Icons.account_circle),
                    prefixText: "@",
                    hintText: "username:homeserver"),
              ),
              SizedBox(height: 16),
              Text(
                "Your username is ${Matrix.of(context).client.userID}",
                style: TextStyle(
                  color: Colors.blueGrey,
                  fontSize: 12,
                ),
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close".toUpperCase(),
                  style: TextStyle(color: Colors.blueGrey)),
              onPressed: () {
                Navigator.of(innerContext).pop();
              },
            ),
            FlatButton(
              child: Text("Continue".toUpperCase()),
              onPressed: () async {
                final String roomID = await client
                    .createRoom(invite: [User("@" + controller.text)]);
                Navigator.of(innerContext).pop();
                if (roomID != null)
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Chat(roomID)),
                  );
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    client = Matrix.of(context).client;

    return AdaptivePageLayout(
      secondScaffold: Scaffold(
        body: Center(
          child: Icon(Icons.chat, size: 100, color: Color(0xFF5625BA)),
        ),
      ),
      firstScaffold: Scaffold(
        appBar: AppBar(
          title: Text(
            "FluffyWeb",
          ),
          leading: IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              Matrix.of(context).clean();
              client.logout();
              Navigator.pushReplacement(context, FadeRoute(page: LoginPage()));
            },
          ),
          elevation: 1,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Color(0xFF5625BA),
          onPressed: () => startChatDialog(),
        ),
        body: FutureBuilder<List<Room>>(
          future: getRooms(context),
          builder: (BuildContext context, snapshot) {
            if (snapshot.hasData) {
              List<Room> rooms = snapshot.data;
              return ListView.builder(
                itemCount: rooms.length,
                itemBuilder: (BuildContext context, int i) => Material(
                  color: widget.activeChat == rooms[i].id
                      ? Color(0xFFE8E8E8)
                      : Colors.white,
                  child: ListTile(
                    leading: Avatar(rooms[i].avatar, client),
                    title: Text(rooms[i].displayname),
                    subtitle: Text(
                      rooms[i].lastMessage,
                      maxLines: 1,
                    ),
                    onTap: () {
                      if (widget.activeChat != null)
                        Navigator.pushReplacement(
                          context,
                          FadeRoute(page: Chat(rooms[i].id)),
                        );
                      else
                        Navigator.push(
                          context,
                          FadeRoute(page: Chat(rooms[i].id)),
                        );
                    },
                    trailing: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text(rooms[i].timeCreated.toEventTimeString()),
                          rooms[i].notificationCount > 0
                              ? Container(
                                  width: 20,
                                  height: 20,
                                  margin: EdgeInsets.only(top: 3),
                                  decoration: BoxDecoration(
                                    color: rooms[i].highlightCount > 0
                                        ? Colors.red
                                        : Color(0xFF5625BA),
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Center(
                                    child: Text(
                                      rooms[i].notificationCount.toString(),
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                )
                              : Text(" "),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            } else
              return Center(
                child: CircularProgressIndicator(),
              );
          },
        ),
      ),
    );
  }
}
