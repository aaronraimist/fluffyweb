import 'package:flutter/material.dart';

class SimpleLayout extends StatelessWidget {
  final Widget primaryPage;
  final Widget secondaryPage;

  const SimpleLayout({this.primaryPage, this.secondaryPage, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        primaryPage,
        MaterialApp(home: secondaryPage),
      ],
    );
  }
}
