import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final MxContent mxContent;
  final Client client;
  final double size;

  const Avatar(this.mxContent, this.client, {this.size = 40, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundImage: mxContent.mxc?.isNotEmpty ?? false
          ? NetworkImage(
              mxContent.getThumbnail(client, width: size, height: size))
          : null,
      backgroundColor: Color(0xFFF8F8F8),
      child: mxContent.mxc.isEmpty
          ? Text("@", style: TextStyle(color: Colors.blueGrey))
          : null,
    );
  }
}
