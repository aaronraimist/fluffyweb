import 'package:famedlysdk/famedlysdk.dart';
import 'package:fluffyweb/ChatList.dart';
import 'package:fluffyweb/Login.dart';
import 'package:fluffyweb/Matrix.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Matrix(
      clientName: "FluffyWeb",
      child: MaterialApp(
        title: 'FluffyWeb',
        theme: ThemeData(
          primaryColor: Color(0xFF5625BA),
          backgroundColor: Colors.white,
          scaffoldBackgroundColor: Colors.white,
          appBarTheme: AppBarTheme(
            color: Colors.white,
            textTheme: TextTheme(
              title: TextStyle(color: Colors.black),
            ),
            iconTheme: IconThemeData(color: Colors.black),
          ),
        ),
        home: Builder(
          builder: (BuildContext context) => StreamBuilder<LoginState>(
              stream: Matrix.of(context)
                  .client
                  .connection
                  .onLoginStateChanged
                  .stream,
              builder: (context, snapshot) {
                if (Matrix.of(context).client.isLogged()) return ChatList();
                return LoginPage();
              }),
        ),
      ),
    );
  }
}
