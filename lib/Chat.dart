import 'package:famedlysdk/famedlysdk.dart';
import 'package:fluffyweb/AdaptivePageLayout.dart';
import 'package:fluffyweb/Avatar.dart';
import 'package:fluffyweb/ChatList.dart';
import 'package:fluffyweb/Matrix.dart';
import 'package:flutter/material.dart';

class Chat extends StatefulWidget {
  final String id;

  const Chat(this.id, {Key key}) : super(key: key);
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  Room room;

  Timeline timeline;

  Future<bool> getTimeline() async {
    timeline ??= await room.getTimeline(onUpdate: () {
      setState(() {});
    });
    return true;
  }

  @override
  void dispose() {
    timeline?.sub?.cancel();
    super.dispose();
  }

  final TextEditingController sendController = TextEditingController();

  void send() {
    if (sendController.text.isEmpty) return;
    room.sendTextEvent(sendController.text);
    sendController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    Client client = Matrix.of(context).client;
    room ??= client.getRoomById(widget.id);

    if (room.membership == Membership.invite) room.join();

    return AdaptivePageLayout(
      primaryPage: FocusPage.SECOND,
      firstScaffold: ChatList(
        activeChat: widget.id,
      ),
      secondScaffold: Scaffold(
        appBar: AppBar(
          title: Text(room.displayname),
          elevation: 1,
        ),
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                child: FutureBuilder<bool>(
                  future: getTimeline(),
                  builder: (BuildContext context, snapshot) {
                    if (!snapshot.hasData)
                      return Center(
                        child: Text("Loading..."),
                      );
                    if (room.notificationCount != null &&
                        room.notificationCount > 0 &&
                        timeline != null &&
                        timeline.events.length > 0)
                      room.sendReadReceipt(timeline.events[0].eventId);
                    return ListView.builder(
                      reverse: true,
                      itemCount: timeline.events.length,
                      itemBuilder: (BuildContext context, int i) {
                        Event event = timeline.events[i];
                        final bool ownMessage = event.senderId == client.userID;
                        if (event.typeKey == "m.room.message")
                          return ListTile(
                            leading: ownMessage
                                ? null
                                : Avatar(event.sender.avatarUrl, client),
                            trailing: !ownMessage
                                ? null
                                : Avatar(event.sender.avatarUrl, client),
                            subtitle: Row(
                                mainAxisAlignment: ownMessage
                                    ? MainAxisAlignment.end
                                    : MainAxisAlignment.start,
                                children: [
                                  Text(
                                    event.sender.calcDisplayname(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(" - ",
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.grey)),
                                  Text(
                                    event.time.toEventTimeString(),
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                ]),
                            title: Column(
                              crossAxisAlignment: ownMessage
                                  ? CrossAxisAlignment.end
                                  : CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    color: ownMessage
                                        ? Color(0xFF5625BA)
                                        : Colors.white,
                                    border: Border.all(
                                        color: Colors.grey, width: 0.25),
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Text(
                                    event.getBody(),
                                    style: TextStyle(
                                        color: ownMessage
                                            ? Colors.white
                                            : Colors.black),
                                  ),
                                ),
                                SizedBox(height: 2),
                              ],
                            ),
                          );
                        else
                          return Center(
                              child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Text(
                                "${event.sender.calcDisplayname()} sent a ${event.typeKey} event",
                                style: TextStyle(color: Colors.blueGrey)),
                          ));
                      },
                    );
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: Offset(0, -1), // changes position of shadow
                    ),
                  ],
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.attachment),
                      onPressed: () {},
                    ),
                    SizedBox(width: 8),
                    Expanded(
                        child: TextField(
                      onSubmitted: (t) => send(),
                      controller: sendController,
                      decoration: InputDecoration(
                        labelText: "Deine Nachricht",
                        border: InputBorder.none,
                      ),
                    )),
                    SizedBox(width: 8),
                    IconButton(
                      icon: Icon(Icons.send),
                      onPressed: () => send(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
