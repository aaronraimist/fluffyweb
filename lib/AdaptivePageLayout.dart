import 'package:flutter/material.dart';

enum FocusPage { FIRST, SECOND }

class AdaptivePageLayout extends StatelessWidget {
  final Widget firstScaffold;
  final Widget secondScaffold;
  final FocusPage primaryPage;
  final double minWidth;

  AdaptivePageLayout(
      {this.firstScaffold,
      this.secondScaffold,
      this.primaryPage = FocusPage.FIRST,
      this.minWidth = 400,
      Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      if (orientation == Orientation.portrait ||
          MediaQuery.of(context).size.width <
              minWidth * 2) if (primaryPage == FocusPage.FIRST)
        return firstScaffold;
      else
        return secondScaffold;
      return Row(
        children: <Widget>[
          Container(
            width: minWidth,
            child: firstScaffold,
          ),
          Container(
            width: 1,
            color: Color(0xFFE8E8E8),
          ),
          Expanded(
            child: Container(
              child: secondScaffold,
            ),
          )
        ],
      );
    });
  }
}
